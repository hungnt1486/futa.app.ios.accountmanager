//
//  PhoneExisted.swift
//  FUTACustomer
//
//  Created by mac on 23/05/2022.
//  Copyright © 2022 FUTA Group. All rights reserved.
//

import Foundation

public struct PhoneExistedEntities: Decodable {
    public var name: String
    public var is_exists: Bool
}
