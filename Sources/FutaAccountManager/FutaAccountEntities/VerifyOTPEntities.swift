//
//  VerifyOTP.swift
//  FUTACustomer
//
//  Created by mac on 26/05/2022.
//  Copyright © 2022 FUTA Group. All rights reserved.
//

public struct VerifyOTPEntities: Decodable {
    public var customToken: String
}
