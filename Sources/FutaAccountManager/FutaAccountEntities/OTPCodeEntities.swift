//
//  OTPCodeResponse.swift
//  FUTACustomer
//
//  Created by mac on 25/05/2022.
//  Copyright © 2022 FUTA Group. All rights reserved.
//

import Foundation

public struct OTPCodeEntities: Decodable {
    public var retryAt: Double = 0
    public var nextService: Int = 0
    public var sessionId = ""
    public var countDownTime: Double = 0
}
