

import Foundation
import RxSwift
import Alamofire
import FutaCoreAPI


public class FutaAccountManager: NSObject {
    
    public static var shared: FutaAccountManager {
        let instance = FutaAccountManager()
        return instance
    }
    
    public var futaAccountService = FutaAccountServiceImpl()
    public var futaAuthenService = FutaAuthenServiceImpl()
    
    
}
