//
//  AccountService.swift
//  FUTACustomer
//
//  Created by Vu Mai Hoang Hai Hung on 21/05/2022.
//  Copyright © 2022 FUTA Group. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
import FutaCoreAPI

public protocol FutaAccountService {
    func checkPhoneExisted(phoneNumber: String) -> RxSwift.Observable<PhoneExistedEntities>
    func setPassword(password: String, userId: Int) -> Observable<Void>
    func changePassword(oldPassword: String, newPassword: String, userId: Int) -> Observable<Void>
    func deactiveAccount() -> Observable<Bool>
}

public struct FutaAccountServiceImpl: FutaAccountService {
    
    public init(){}

    public func checkPhoneExisted(phoneNumber: String) -> RxSwift.Observable<PhoneExistedEntities> {
        
        return FutaAccountAPIEndPoint.checkPhoneExisted(phoneNumber: phoneNumber).call()
    }
    
    public func setPassword(password: String, userId: Int) -> Observable<Void> {
        return FutaAccountAPIEndPoint.setPassword(password: password, userId: userId).call()
    }
    public func changePassword(oldPassword: String, newPassword: String, userId: Int) -> Observable<Void> {
        return FutaAccountAPIEndPoint.changePassword(oldPassword: oldPassword, newPassword: newPassword, userId: userId).call()
    }
    
    public func deactiveAccount() -> Observable<Bool> {
        
        return FutaAccountAPIEndPoint.deactiveAccount.call()
    }
}

enum FutaAccountAPIEndPoint {
    case checkPhoneExisted(phoneNumber: String)
    case setPassword(password: String, userId: Int)
    case changePassword(oldPassword: String, newPassword: String, userId: Int)
    case deactiveAccount
}


extension FutaAccountAPIEndPoint: FutaAPICall {
    var interceptor: Alamofire.RequestInterceptor {
        return FutaHttpInterceptor()
    }
    
    
    var domain: FutaAppEnvironment.FutaDomain {
        return .vato
    }
    
    var path: String {
        switch (self) {
        case .checkPhoneExisted(let phoneNumber):
            return "/authenticate/check-phone-existed/\(phoneNumber)"
        case .setPassword(_, let userId):
            return "/user/\(userId)/set-password"
        case .changePassword(_, _, let userId):
            return "/user/\(userId)/password"
        case .deactiveAccount:
            return "/user/ban"
        }
     }
    
    var method: Alamofire.HTTPMethod {
        switch(self) {
        case .checkPhoneExisted, .setPassword, .deactiveAccount:
            return .post
        case .changePassword:
            return .put
        }
    }
    
    var urlParams: [String : Any]? {
        switch(self) {
        case .setPassword(let password, _):
            return ["password": password, "password_confirm": password, "provider": "PHONE"]
        case.changePassword (let oldPassword, let newPassword, _):
            return ["old_password": oldPassword, "new_password": newPassword, "provider": "PHONE"]
        default:
            return nil
        }
    }
    var paramEncoding: ParameterEncoding {
        switch(self) {
        case .setPassword:
            return JSONEncoding.default
        case .changePassword:
            return JSONEncoding.default
        default:
            return URLEncoding.queryString
        }
    }
    
    var headers: HTTPHeaders {
        var additionalHeader = baseHeader
        switch self {
        case .checkPhoneExisted:
            let header = HTTPHeader(name: "Authorization", value: "Bearer \(FutaCoreAPIConfig.userToken)")
            additionalHeader.add(header)
            // comment
        case .setPassword, .changePassword, .deactiveAccount:
            let header = HTTPHeader(name: "x-access-token", value: FutaCoreAPIConfig.userToken)
            additionalHeader.add(header)
        }
        
        return additionalHeader
    }
}
