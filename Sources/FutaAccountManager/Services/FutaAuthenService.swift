//
//  AuthenService.swift
//  FUTACustomer
//
//  Created by mac on 25/05/2022.
//  Copyright © 2022 FUTA Group. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire
import FutaCoreAPI

public protocol FutaAuthenService {
    func requestOTPCode(phoneNumber: String, devideId: String) -> Observable<OTPCodeEntities>
    func verifyOTPCode(otpCode: String, sessionId: String) -> Observable<VerifyOTPEntities>
    func login(phoneNumer: String, password: String) -> Observable<VerifyOTPEntities>
}

public struct FutaAuthenServiceImpl: FutaAuthenService {
    
    public init() {}

    public func verifyOTPCode(otpCode: String, sessionId: String) -> Observable<VerifyOTPEntities> {
        return FutaAuthenAPIEndPoint.verifyOTPCode(otpCode: otpCode, sessionId: sessionId).call()
    }
    
    public func requestOTPCode(phoneNumber: String, devideId: String) -> Observable<OTPCodeEntities> {
        return FutaAuthenAPIEndPoint.requestOTPCode(phoneNumber: phoneNumber, deviceId: devideId).call()
    }
    
    public func login(phoneNumer: String, password: String) -> Observable<VerifyOTPEntities> {
        return FutaAuthenAPIEndPoint.login(phoneNumber: phoneNumer, password: password).call()
    }
}

enum FutaAuthenAPIEndPoint {
    case requestOTPCode(phoneNumber: String, deviceId: String)
    case verifyOTPCode(otpCode: String, sessionId: String)
    case login(phoneNumber: String, password: String)
}

extension FutaAuthenAPIEndPoint: FutaAPICall {
    var interceptor: Alamofire.RequestInterceptor {
        return FutaHttpInterceptor()
    }
    
    
    var domain: FutaAppEnvironment.FutaDomain {
        return .vato
    }
    
    var path: String {
        switch(self) {
        case .requestOTPCode:
            return "/authenticate/request_code"
        case .verifyOTPCode:
            return "/authenticate/verify_code"
        case .login:
            return "/authenticate/login"
        }
    }
    
    var method: Alamofire.HTTPMethod {
        return .post
    }
    
    var headers: HTTPHeaders {
        var additionalHeader = baseHeader
        switch self {
        case .requestOTPCode, .verifyOTPCode, .login:
            let header = HTTPHeader(name: "Authorization", value: "Bearer \(FutaCoreAPIConfig.userToken)")
            additionalHeader.add(header)
//        default:
//            let header = HTTPHeader(name: "x-access-token", value: FutaCoreAPIConfig.userToken)
//            additionalHeader.add(header)
//            break
        }
        
        return additionalHeader
    }
    
    var urlParams: [String : Any]? {
        switch(self) {
        case .requestOTPCode(let phoneNumber, let deviceId):
            return ["phoneNumber": phoneNumber, "deviceId": deviceId]
        case .verifyOTPCode(let otpCode, let sessionId):
            return ["verifyCode": otpCode, "sessionId": sessionId]
        case .login(let phoneNumber, let password):
            return ["grant_type": "PASSWORD", "provider": "PHONE", "password": password, "username": phoneNumber]
        }
    }
    
    var paramEncoding: ParameterEncoding {
        return JSONEncoding.default
    }
    
    
}
